package me.liammcguire.demobox.spring.springwebfluxdocumentation.controller.customer

import com.mongodb.client.result.DeleteResult
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import me.liammcguire.demobox.spring.springwebfluxdocumentation.documentation.DocumentationTest
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.NewAddress
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.NewCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.StoredAddress
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.StoredCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.UpdateAddress
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.UpdateCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.repository.customer.CustomerRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.ApplicationContext
import org.springframework.http.MediaType
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@WebFluxTest(CustomerController::class)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
@ExtendWith(RestDocumentationExtension::class, SpringExtension::class)
class CustomerControllerDocumentationTest(
    @Autowired private val applicationContext: ApplicationContext
) : DocumentationTest() {

    @MockkBean
    private lateinit var customerRepository: CustomerRepository
    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun setup(restDocumentation: RestDocumentationContextProvider) {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext)
            .configureClient()
            .filter(WebTestClientRestDocumentation.documentationConfiguration(restDocumentation))
            .build()
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun generateCustomerDocumentationCreate() {
        val storedCustomer = createStoredCustomer().copy(id = null)
        val newCustomer = createNewCustomer()
        every { customerRepository.store(storedCustomer) } returns Mono.just(storedCustomer.copy(
            id = "5e2234051dfd0b58632c48ed"
        ))

        webTestClient.post()
            .uri("/api/customer/create")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(newCustomer)
            .exchange()
            .expectStatus().isCreated
            .expectBody()
            .consumeWith(createDocumentation(
                "customer-create",
                withJsonBody(listOf(
                    forenameDocumentation(nullable = false),
                    surnameDocumentation(nullable = false),
                    emailAddressDocumentation(nullable = false),
                    contactNumberDocumentation(nullable = false),
                    addressStreetDocumentation(nullable = false),
                    addressCityDocumentation(nullable = false),
                    addressPostcodeDocumentation(nullable = false)
                )),
                withJsonResponse(createStoredCustomer(), "Customer")
            ))
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun generateCustomerDocumentationList() {
        val storedCustomer = createStoredCustomer()
        every { customerRepository.list(StoredCustomer::class) } returns Flux.fromArray(arrayOf(storedCustomer))

        webTestClient.get()
            .uri("/api/customer/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(StoredCustomer::class.java)
            .hasSize(1)
            .consumeWith<WebTestClient.ListBodySpec<StoredCustomer>>(createDocumentation(
                "customer-list",
                withJsonResponse(arrayOf(createStoredCustomer()), "Customer")
            ))
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun generateCustomerDocumentationView() {
        val storedCustomer = createStoredCustomer()
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(storedCustomer)

        webTestClient.get()
            .uri("/api/customer/view/{id}", "5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .consumeWith(createDocumentation(
                "customer-view",
                withPathVariable(mapOf("id" to "A customers unique identifier")),
                withJsonResponse(createStoredCustomer(), "Customer")
            ))
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun generateCustomerDocumentationRemove() {
        val storedCustomer = createStoredCustomer()
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(storedCustomer)
        every {
            customerRepository.remove(storedCustomer)
        } returns Mono.just(DeleteResult.acknowledged(1))

        webTestClient.delete()
            .uri("/api/customer/delete/{id}", "5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .consumeWith(createDocumentation(
                "customer-remove",
                withPathVariable(mapOf("id" to "A customers unique identifier"))
            ))
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun generateCustomerDocumentationUpdate() {
        val storedCustomer = createStoredCustomer()
        val updateCustomer = createUpdateCustomer()
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(storedCustomer)
        every { customerRepository.store(any()) } returns Mono.just(storedCustomer.copy(
            emailAddress = "john.smith123@localhost.com",
            address = storedCustomer.address.copy(
                street = "321 Sesame Street"
            )
        ))

        webTestClient.put()
            .uri("/api/customer/update/{id}", "5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateCustomer)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .consumeWith(createDocumentation(
                "customer-update",
                withJsonBody(listOf(
                    forenameDocumentation(nullable = true),
                    surnameDocumentation(nullable = true),
                    emailAddressDocumentation(nullable = true),
                    contactNumberDocumentation(nullable = true),
                    addressStreetDocumentation(nullable = true),
                    addressCityDocumentation(nullable = true),
                    addressPostcodeDocumentation(nullable = true)
                )),
                withPathVariable(mapOf("id" to "A customers unique identifier")),
                withJsonResponse(createStoredCustomer(), "Customer")
            ))
    }

    companion object {
        private fun createStoredCustomer() = StoredCustomer(
            id = "5e2234051dfd0b58632c48ed",
            forename = "John",
            surname = "Smith",
            emailAddress = "john.smith@localhost.com",
            contactNumber = "07123456789",
            address = StoredAddress(
                street = "123 Sesame Street",
                city = "Manhattan",
                postcode = "NE66 1FE"
            )
        )

        private fun createNewCustomer() = NewCustomer(
            forename = "John",
            surname = "Smith",
            emailAddress = "john.smith@localhost.com",
            contactNumber = "07123456789",
            address = NewAddress(
                street = "123 Sesame Street",
                city = "Manhattan",
                postcode = "NE66 1FE"
            )
        )

        private fun createUpdateCustomer() = UpdateCustomer(
            forename = "John",
            surname = "Smith",
            emailAddress = "john.smith@localhost.com",
            contactNumber = "07123456789",
            address = UpdateAddress(
                street = "123 Sesame Street",
                city = "Manhattan",
                postcode = "NE66 1FE"
            )
        )

        private fun forenameDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "forename",
            type = String::class,
            description = "A Customer's forename cannot${if (nullable) " " else " be blank, "}have a length " +
                "exceeding 50 characters or have any special characters"
        )

        private fun surnameDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "surname",
            type = String::class,
            description = "A Customer's surname cannot${if (nullable) " " else " be blank, "}have a " +
                "length exceeding 50 characters or have any special characters"
        )

        private fun emailAddressDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "emailAddress",
            type = String::class,
            description = "A Customer's email address${if (nullable) " " else "cannot be blank and "}must be" +
                "a valid email address"
        )

        private fun contactNumberDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "contactNumber",
            type = String::class,
            description = "A Customer's contact number${if (nullable) " " else "cannot be blank and "}must be" +
                "a valid UK mobile number"
        )

        private fun addressStreetDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "address.street",
            type = String::class,
            description = "An Address's street${if (nullable) " " else "cannot be blank, "}have a " +
                "length exceeding 50 characters or have any special characters"
        )

        private fun addressCityDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "address.city",
            type = String::class,
            description = "An Address's city${if (nullable) " " else "cannot be blank, "}have a " +
                "length exceeding 50 characters or have any special characters"
        )

        private fun addressPostcodeDocumentation(nullable: Boolean) = FieldDocumentation(
            jsonPath = "address.postcode",
            type = String::class,
            description = "An Address's postcode${if (nullable) " " else "cannot be blank and "}must be " +
                "a valid UK postcode"
        )
    }
}
