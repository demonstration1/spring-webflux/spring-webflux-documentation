package me.liammcguire.demobox.spring.springwebfluxdocumentation.controller.customer

import com.mongodb.client.result.DeleteResult
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.NewAddress
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.NewCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.StoredAddress
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.StoredCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.UpdateAddress
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.UpdateCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.repository.customer.CustomerRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@WebFluxTest(CustomerController::class)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class CustomerControllerIntegrationTest(
    @Autowired private val webTestClient: WebTestClient
) {

    @MockkBean
    private lateinit var customerRepository: CustomerRepository

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Creating a new Customer should return a 200 response`() {
        val storedCustomer = createStoredCustomer().copy(id = null)
        val newCustomer = createNewCustomer()
        every { customerRepository.store(storedCustomer) } returns Mono.just(storedCustomer.copy(
            id = "5e2234051dfd0b58632c48ed"
        ))

        val responseBody = webTestClient.post()
            .uri("/api/customer/create")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(newCustomer)
            .exchange()
            .expectStatus().isCreated
            .expectBody(StoredCustomer::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!
        assertEquals("5e2234051dfd0b58632c48ed", customer.id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("123 Sesame Street", address.street)
        assertEquals("Manhattan", address.city)
        assertEquals("NE66 1FE", address.postcode)

        verify(exactly = 1) { customerRepository.store(storedCustomer) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Creating a new Customer should return a 400 response when an invalid request is made`() {
        val newCustomer = createNewCustomer().copy(
            forename = null, surname = "£\$DD£\$DAS", address = null
        )

        webTestClient.post()
            .uri("/api/customer/create")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(newCustomer)
            .exchange()
            .expectStatus().isBadRequest

        verify(exactly = 0) { customerRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Listing all Customers should return a 200 response`() {
        every { customerRepository.list(StoredCustomer::class) } returns Flux.fromArray(arrayOf(createStoredCustomer()))

        val responseBody = webTestClient.get()
            .uri("/api/customer/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(StoredCustomer::class.java)
            .hasSize(1)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!.first()
        assertEquals("5e2234051dfd0b58632c48ed", customer.id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("123 Sesame Street", address.street)
        assertEquals("Manhattan", address.city)
        assertEquals("NE66 1FE", address.postcode)

        verify(exactly = 1) { customerRepository.list(StoredCustomer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Listing all Customers should return a 404 response when no Customers could be found`() {
        every { customerRepository.list(StoredCustomer::class) } returns Flux.empty()

        webTestClient.get()
            .uri("/api/customer/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .isEmpty

        verify(exactly = 1) { customerRepository.list(StoredCustomer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Viewing a Customer should return a 200 response`() {
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(createStoredCustomer())

        val responseBody = webTestClient.get()
            .uri("/api/customer/view/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(StoredCustomer::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!
        assertEquals("5e2234051dfd0b58632c48ed", customer.id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("123 Sesame Street", address.street)
        assertEquals("Manhattan", address.city)
        assertEquals("NE66 1FE", address.postcode)

        verify(exactly = 1) { customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Viewing a Customer should return a 404 response when a Customer couldn't be found`() {
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.empty()

        webTestClient.get()
            .uri("/api/customer/view/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .isEmpty

        verify(exactly = 1) { customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Removing a Customer should return a 200 response`() {
        val storedCustomer = createStoredCustomer()
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(storedCustomer)
        every {
            customerRepository.remove(storedCustomer)
        } returns Mono.just(DeleteResult.acknowledged(1))

        webTestClient.delete()
            .uri("/api/customer/delete/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .isEmpty

        verify(exactly = 1) {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
            customerRepository.remove(storedCustomer)
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Removing a Customer should return a 404 response when a Customer couldn't be found`() {
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.empty()

        webTestClient.delete()
            .uri("/api/customer/delete/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody()
            .isEmpty

        verify(exactly = 1) { customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class) }
        verify(exactly = 0) { customerRepository.remove(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Updating an existing Customer should return a 200 response`() {
        val storedCustomer = createStoredCustomer()
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(storedCustomer)
        every { customerRepository.store(any()) } returns Mono.just(storedCustomer.copy(
            emailAddress = "john.smith123@localhost.com",
            address = storedCustomer.address.copy(
                street = "321 Sesame Street"
            )
        ))

        val responseBody = webTestClient.put()
            .uri("/api/customer/update/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(createUpdateCustomer())
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(StoredCustomer::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!
        assertEquals("5e2234051dfd0b58632c48ed", customer.id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith123@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("321 Sesame Street", address.street)
        assertEquals("Manhattan", address.city)
        assertEquals("NE66 1FE", address.postcode)

        verify(exactly = 1) {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
            customerRepository.store(any())
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Updating an existing Customer should return a 404 response when a Customer couldn't be found`() {
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.empty()

        webTestClient.put()
            .uri("/api/customer/update/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(createUpdateCustomer())
            .exchange()
            .expectStatus().isNotFound
            .expectBody()
            .isEmpty

        verify(exactly = 1) { customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class) }
        verify(exactly = 0) { customerRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Updating an existing Customer should return a 400 response when an invalid request is made`() {
        every {
            customerRepository.viewById("5e2234051dfd0b58632c48ed", StoredCustomer::class)
        } returns Mono.just(createStoredCustomer())
        val updateCustomer = createUpdateCustomer().copy(
            surname = "£\$DD£\$DAS", address = UpdateAddress(street = "&*&£*$&")
        )

        webTestClient.put()
            .uri("/api/customer/update/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateCustomer)
            .exchange()
            .expectStatus().isBadRequest

        verify(exactly = 0) { customerRepository.store(any()) }
    }

    companion object {
        internal fun createStoredCustomer(): StoredCustomer = StoredCustomer(
            id = "5e2234051dfd0b58632c48ed",
            forename = "John",
            surname = "Smith",
            emailAddress = "john.smith@localhost.com",
            contactNumber = "07123456789",
            address = StoredAddress(
                street = "123 Sesame Street",
                city = "Manhattan",
                postcode = "NE66 1FE"
            )
        )

        internal fun createNewCustomer(): NewCustomer = NewCustomer(
            forename = "John",
            surname = "Smith",
            emailAddress = "john.smith@localhost.com",
            contactNumber = "07123456789",
            address = NewAddress(
                street = "123 Sesame Street",
                city = "Manhattan",
                postcode = "NE66 1FE"
            )
        )

        internal fun createUpdateCustomer(): UpdateCustomer = UpdateCustomer(
            emailAddress = "john.smith123@localhost.com",
            address = UpdateAddress(
                street = "321 Sesame Street"
            )
        )
    }
}
