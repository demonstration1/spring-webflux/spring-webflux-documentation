=== Removing a Customer

==== Request Structure

include::{snippets}/customer-remove/http-request.adoc[]

==== Path parameters

include::{snippets}/customer-remove/path-parameters.adoc[]

==== Example response

include::{snippets}/customer-remove/http-response.adoc[]

==== Example curl request

include::{snippets}/customer-remove/curl-request.adoc[]