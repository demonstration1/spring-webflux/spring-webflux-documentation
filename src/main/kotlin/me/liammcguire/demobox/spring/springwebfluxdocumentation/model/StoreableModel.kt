package me.liammcguire.demobox.spring.springwebfluxdocumentation.model

interface StoreableModel {
    val id: String?
}
