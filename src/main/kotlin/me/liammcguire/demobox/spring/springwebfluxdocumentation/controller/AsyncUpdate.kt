package me.liammcguire.demobox.spring.springwebfluxdocumentation.controller

import kotlin.reflect.KClass
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxdocumentation.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono

interface AsyncUpdate<SM : StoreableModel> : RepositoryDependency<SM> {
    fun update(id: String, type: KClass<SM>, applyUpdate: (SM) -> SM): Mono<ResponseEntity<SM>> =
        repository.viewById(id, type)
            .flatMap { entity ->
                repository.store(applyUpdate(entity))
                    .map { updatedEntity -> buildResponse(HttpStatus.OK, updatedEntity) }
            }
            .switchIfEmpty(Mono.just(buildResponse(HttpStatus.NOT_FOUND)))
}
