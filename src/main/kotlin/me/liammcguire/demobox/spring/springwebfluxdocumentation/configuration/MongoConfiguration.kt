package me.liammcguire.demobox.spring.springwebfluxdocumentation.configuration

import com.mongodb.ConnectionString
import me.liammcguire.demobox.spring.springwebfluxdocumentation.repository.customer.CustomerRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory

@Configuration
class MongoConfiguration(
    @Value("\${spring.data.mongodb.uri}") private val connectionString: String
) {

    @Bean
    fun reactiveMongoTemplate(): ReactiveMongoTemplate =
        ReactiveMongoTemplate(SimpleReactiveMongoDatabaseFactory(ConnectionString(connectionString)))

    @Bean
    fun customerRepository(): CustomerRepository = CustomerRepository(reactiveMongoTemplate())
}
