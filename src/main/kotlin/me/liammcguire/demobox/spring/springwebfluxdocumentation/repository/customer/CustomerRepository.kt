package me.liammcguire.demobox.spring.springwebfluxdocumentation.repository.customer

import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.StoredCustomer
import me.liammcguire.demobox.spring.springwebfluxdocumentation.repository.Repository as AbstractRepository
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

class CustomerRepository(
    reactiveMongoTemplate: ReactiveMongoTemplate
) : AbstractRepository<StoredCustomer>(reactiveMongoTemplate)
