package me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_CITY_INVALID_CHARACTERS
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_CITY_LENGTH
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_CITY_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_POST_CODE_INVALID
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_POST_CODE_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_STREET_INVALID_CHARACTERS
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_STREET_LENGTH
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.AddressValidationError.ERROR_STREET_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.validation.ValidationRegex.REGEX_ALPHANUMERIC_WITH_SPACE
import me.liammcguire.demobox.spring.springwebfluxdocumentation.validation.ValidationRegex.REGEX_POST_CODE
import org.hibernate.validator.constraints.Length

interface Address {
    val street: String?
    val city: String?
    val postcode: String?
}

data class StoredAddress(
    override val street: String,
    override val city: String,
    override val postcode: String
) : Address

data class NewAddress(
    @field:NotBlank(message = ERROR_STREET_MISSING)
    @field:Length(message = ERROR_STREET_LENGTH, max = 50)
    @field:Pattern(message = ERROR_STREET_INVALID_CHARACTERS, regexp = REGEX_ALPHANUMERIC_WITH_SPACE)
    override val street: String? = null,

    @field:NotBlank(message = ERROR_CITY_MISSING)
    @field:Length(message = ERROR_CITY_LENGTH, max = 50)
    @field:Pattern(message = ERROR_CITY_INVALID_CHARACTERS, regexp = REGEX_ALPHANUMERIC_WITH_SPACE)
    override val city: String? = null,

    @field:NotBlank(message = ERROR_POST_CODE_MISSING)
    @field:Pattern(message = ERROR_POST_CODE_INVALID, regexp = REGEX_POST_CODE)
    override val postcode: String? = null
) : Address

data class UpdateAddress(
    @field:Length(message = ERROR_STREET_LENGTH, max = 50)
    @field:Pattern(message = ERROR_STREET_INVALID_CHARACTERS, regexp = REGEX_ALPHANUMERIC_WITH_SPACE)
    override val street: String? = null,

    @field:Length(message = ERROR_CITY_LENGTH, max = 50)
    @field:Pattern(message = ERROR_CITY_INVALID_CHARACTERS, regexp = REGEX_ALPHANUMERIC_WITH_SPACE)
    override val city: String? = null,

    @field:Pattern(message = ERROR_POST_CODE_INVALID, regexp = REGEX_POST_CODE)
    override val postcode: String? = null
) : Address

private object AddressValidationError {
    const val ERROR_STREET_MISSING: String =
        "A street was expected but was not provided."
    const val ERROR_STREET_LENGTH: String =
        "The provided street exceeded the maximum allowed character length of 50."
    const val ERROR_STREET_INVALID_CHARACTERS: String =
        "The provided street contained invalid characters."

    const val ERROR_CITY_MISSING: String =
        "A city was expected but was not provided."
    const val ERROR_CITY_LENGTH: String =
        "The provided city exceeded the maximum allowed character length of 50."
    const val ERROR_CITY_INVALID_CHARACTERS: String =
        "The provided city contained invalid characters."

    const val ERROR_POST_CODE_MISSING: String =
        "A post code was expected but was not provided."
    const val ERROR_POST_CODE_INVALID: String =
        "The provided post code was invalid."
}
