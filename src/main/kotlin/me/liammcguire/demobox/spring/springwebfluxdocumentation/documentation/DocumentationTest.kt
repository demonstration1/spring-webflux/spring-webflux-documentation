package me.liammcguire.demobox.spring.springwebfluxdocumentation.documentation

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import java.math.BigDecimal
import java.util.function.Consumer
import kotlin.reflect.KClass
import org.apache.commons.lang3.StringUtils
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders
import org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import org.springframework.restdocs.payload.RequestFieldsSnippet
import org.springframework.restdocs.payload.ResponseFieldsSnippet
import org.springframework.restdocs.request.PathParametersSnippet
import org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import org.springframework.restdocs.request.RequestDocumentation.pathParameters
import org.springframework.restdocs.request.RequestDocumentation.requestParameters
import org.springframework.restdocs.request.RequestParametersSnippet
import org.springframework.restdocs.snippet.Snippet
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation
import org.springframework.test.web.reactive.server.EntityExchangeResult

abstract class DocumentationTest {

    protected fun <T : Any> createDocumentation(
        apiName: String,
        vararg snippet: Snippet = emptyArray()
    ): Consumer<EntityExchangeResult<T>> = WebTestClientRestDocumentation.document<EntityExchangeResult<T>>(
        apiName,
        preprocessRequest(removeHeaders("Host", "Content-Length"), prettyPrint()),
        preprocessResponse(prettyPrint()),
        *snippet
    )

    protected fun withPathVariable(pathVariable: Map<String, String>): PathParametersSnippet = pathParameters(
        pathVariable.map { (field, description) -> parameterWithName(field).description(description) })

    protected fun withRequestVariable(requestVariable: Map<String, String>): RequestParametersSnippet = requestParameters(
        requestVariable.map { (field, description) -> parameterWithName(field).description(description) })

    protected fun withJsonBody(entity: List<FieldDocumentation>): RequestFieldsSnippet =
        requestFields(entity.map { fieldDocumentation ->
            fieldWithPath(fieldDocumentation.jsonPath)
                .type(fieldDocumentation.type)
                .description(fieldDocumentation.description)
        })

    protected fun <T : Any> withJsonResponse(entity: T, entityName: String): ResponseFieldsSnippet =
        responseFields(investigateEntity(entity, entityName).map { fieldDocumentation ->
            fieldWithPath(fieldDocumentation.jsonPath)
                .type(fieldDocumentation.type)
                .description(fieldDocumentation.description)
        })

    /**
     * Function is responsible for probing a given instance of T and generating a series of generic
     * documentation details for it.
     */
    private fun <T : Any> investigateEntity(entity: T, entityName: String): List<FieldDocumentation> =
        traverseNode(
            when (entity) {
                is Collection<*> -> entityToNode(entity.first()
                    ?: throw IllegalArgumentException("The provided entity was an empty collection."))
                is Array<*> -> entityToNode(entity.first()
                    ?: throw IllegalArgumentException("The provided entity was an empty array."))
                else -> entityToNode(entity)
            }, entityName
        ).map { fieldDocumentation ->
            when (entity) {
                is Collection<*> -> fieldDocumentation.copy(jsonPath = "[0].${fieldDocumentation.jsonPath}")
                is Array<*> -> fieldDocumentation.copy(jsonPath = "[0].${fieldDocumentation.jsonPath}")
                else -> fieldDocumentation
            }
        }.toList()

    /**
     * Utility function is responsible for converting a given instance of T into a traversable
     * instance of JsonNode.
     */
    private fun <T : Any> entityToNode(entity: T): JsonNode = ObjectMapper()
        .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        .valueToTree(entity)

    /**
     * Recursive utility function is responsible for traversing through a provided tree of nodes
     * and calculating respective JSON paths and associated data types.
     */
    private fun traverseNode(
        rootNode: JsonNode,
        entityName: String,
        parentPath: String? = null
    ): Sequence<FieldDocumentation> = rootNode.fields().asSequence().flatMap { (field, node) ->
        when {
            node.fields().asSequence().count() > 1 -> // Is the node a complex data type?
                traverseNode(node as JsonNode, entityName, parentPath?.plus(".$field") ?: field)

            node.isArray ->
                traverseNode(node as JsonNode, entityName, parentPath?.plus(".$field[0]") ?: field)

            else -> {
                val jsonPath = (parentPath?.plus(".$field") ?: field)
                sequenceOf(FieldDocumentation(
                    jsonPath = jsonPath,
                    type = nodeToDataType(node as JsonNode),
                    description = buildGenericDescription(jsonPath, entityName)
                ))
            }
        }
    }

    /**
     * Utility function is responsible for mapping a given node to a JVM recognised data type.
     *
     * @throws IllegalArgumentException when the provided node contains a null or empty value.
     * @throws Exception when the provided node is of an unrecognised type.
     */
    private fun nodeToDataType(node: JsonNode): KClass<*> = when {
        node.isBoolean -> Boolean::class
        node.isLong -> Long::class
        node.isInt -> Int::class
        node.isDouble -> Double::class
        node.isBigDecimal -> BigDecimal::class
        node.isFloat -> Float::class
        node.isTextual -> String::class
        node.isEmpty || node.isNull ->
            throw IllegalArgumentException("The provided node cannot contain null or empty values.")
        else -> throw Exception("The provided node was of an unrecognised type.")
    }

    /**
     * Utility function is responsible for building a generic description based on the provided JSON
     * path and the name of the entity. An example of a potential description would be
     * "A Customer's email address".
     */
    private fun buildGenericDescription(jsonPath: String, entityName: String): String {
        val splitPath = jsonPath.split(".")

        fun formatString(s: String): String = StringUtils.splitByCharacterTypeCamelCase(s)
            .joinToString(" ")
            .toLowerCase()

        return when (splitPath.size > 1) {
            true -> "A ${StringUtils.capitalize(splitPath[splitPath.size - 2])}'s ${formatString(splitPath.last())}"
            false -> "A ${StringUtils.capitalize(entityName)}'s ${formatString(jsonPath)}"
        }
    }

    data class FieldDocumentation(
        val jsonPath: String,
        val type: KClass<*>,
        val description: String
    )
}
