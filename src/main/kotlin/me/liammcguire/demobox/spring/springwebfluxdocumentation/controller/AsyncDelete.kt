package me.liammcguire.demobox.spring.springwebfluxdocumentation.controller

import kotlin.reflect.KClass
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxdocumentation.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono

interface AsyncDelete<SM : StoreableModel> : RepositoryDependency<SM> {
    fun delete(id: String, type: KClass<SM>): Mono<ResponseEntity<Nothing>> = repository.viewById(id, type)
        .flatMap { model ->
            repository.remove(model).map { deleteResult ->
                when (deleteResult.deletedCount == 1L) {
                    true -> buildResponse<Nothing>(HttpStatus.OK)
                    false -> buildResponse(HttpStatus.INTERNAL_SERVER_ERROR)
                }
            }
        }
        .switchIfEmpty(Mono.just(buildResponse(HttpStatus.NOT_FOUND)))
}
