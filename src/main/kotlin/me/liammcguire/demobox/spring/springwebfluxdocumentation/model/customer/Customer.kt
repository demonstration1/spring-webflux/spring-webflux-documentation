package me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_ADDRESS_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_CONTACT_NUMBER_INVALID
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_CONTACT_NUMBER_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_EMAIL_ADDRESS_INVALID
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_EMAIL_ADDRESS_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_FORENAME_INVALID_CHARACTERS
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_FORENAME_LENGTH
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_FORENAME_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_SURNAME_INVALID_CHARACTERS
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_SURNAME_LENGTH
import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.customer.CustomerValidationError.ERROR_SURNAME_MISSING
import me.liammcguire.demobox.spring.springwebfluxdocumentation.validation.ValidationRegex.REGEX_ALPHA_WITH_SPACE
import me.liammcguire.demobox.spring.springwebfluxdocumentation.validation.ValidationRegex.REGEX_MOBILE_NUMBER
import org.hibernate.validator.constraints.Length
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

interface Customer {
    val forename: String?
    val surname: String?
    val emailAddress: String?
    val contactNumber: String?
    val address: Address?
}

@Document(collection = "customers")
data class StoredCustomer(
    @Id override val id: String? = null,
    override val forename: String,
    override val surname: String,
    override val emailAddress: String,
    override val contactNumber: String,
    override val address: StoredAddress
) : Customer, StoreableModel

data class NewCustomer(
    @field:NotBlank(message = ERROR_FORENAME_MISSING)
    @field:Length(message = ERROR_FORENAME_LENGTH, max = 50)
    @field:Pattern(message = ERROR_FORENAME_INVALID_CHARACTERS, regexp = REGEX_ALPHA_WITH_SPACE)
    override val forename: String? = null,

    @field:NotBlank(message = ERROR_SURNAME_MISSING)
    @field:Length(message = ERROR_SURNAME_LENGTH, max = 50)
    @field:Pattern(message = ERROR_SURNAME_INVALID_CHARACTERS, regexp = REGEX_ALPHA_WITH_SPACE)
    override val surname: String? = null,

    @field:NotBlank(message = ERROR_EMAIL_ADDRESS_MISSING)
    @field:Email(message = ERROR_EMAIL_ADDRESS_INVALID)
    override val emailAddress: String? = null,

    @field:NotBlank(message = ERROR_CONTACT_NUMBER_MISSING)
    @field:Pattern(message = ERROR_CONTACT_NUMBER_INVALID, regexp = REGEX_MOBILE_NUMBER)
    override val contactNumber: String? = null,

    @field:NotNull(message = ERROR_ADDRESS_MISSING)
    override val address: NewAddress? = null
) : Customer

data class UpdateCustomer(
    @field:Length(message = ERROR_FORENAME_LENGTH, max = 50)
    @field:Pattern(message = ERROR_FORENAME_INVALID_CHARACTERS, regexp = REGEX_ALPHA_WITH_SPACE)
    override val forename: String? = null,

    @field:Length(message = ERROR_SURNAME_LENGTH, max = 50)
    @field:Pattern(message = ERROR_SURNAME_INVALID_CHARACTERS, regexp = REGEX_ALPHA_WITH_SPACE)
    override val surname: String? = null,

    @field:Email(message = ERROR_EMAIL_ADDRESS_INVALID)
    override val emailAddress: String? = null,

    @field:Pattern(message = ERROR_CONTACT_NUMBER_INVALID, regexp = REGEX_MOBILE_NUMBER)
    override val contactNumber: String? = null,

    override val address: UpdateAddress? = null
) : Customer

private object CustomerValidationError {
    const val ERROR_FORENAME_MISSING: String =
        "A forename was expected but was not provided."
    const val ERROR_FORENAME_LENGTH: String =
        "The provided forename exceeded the maximum allowed character length of 50."
    const val ERROR_FORENAME_INVALID_CHARACTERS: String =
        "The provided forename contained invalid characters."

    const val ERROR_SURNAME_MISSING: String =
        "A surname was expected but was not provided."
    const val ERROR_SURNAME_LENGTH: String =
        "The provided surname exceeded the maximum allowed character length of 50."
    const val ERROR_SURNAME_INVALID_CHARACTERS: String =
        "The provided surname contained invalid characters."

    const val ERROR_EMAIL_ADDRESS_MISSING: String =
        "An email address was expected but was not provided."
    const val ERROR_EMAIL_ADDRESS_INVALID: String =
        "The provided email address was not valid."

    const val ERROR_CONTACT_NUMBER_MISSING: String =
        "A contact number was expected but was not provided."
    const val ERROR_CONTACT_NUMBER_INVALID: String =
        "The provided contact number was not valid."

    const val ERROR_ADDRESS_MISSING: String =
        "An address was expected but was not provided."
}
