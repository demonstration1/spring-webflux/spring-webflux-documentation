package me.liammcguire.demobox.spring.springwebfluxdocumentation

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringWebfluxDocumentation

fun main(args: Array<String>) {
    runApplication<SpringWebfluxDocumentation>(*args)
}
