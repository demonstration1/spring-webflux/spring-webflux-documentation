package me.liammcguire.demobox.spring.springwebfluxdocumentation.controller

import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxdocumentation.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono

interface AsyncCreate<SM : StoreableModel> : RepositoryDependency<SM> {
    fun create(entity: SM): Mono<ResponseEntity<SM>> = repository.store(entity)
        .map { storedEntity -> buildResponse(HttpStatus.CREATED, storedEntity) }
}
