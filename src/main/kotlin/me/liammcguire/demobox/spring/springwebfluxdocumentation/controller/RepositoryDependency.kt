package me.liammcguire.demobox.spring.springwebfluxdocumentation.controller

import me.liammcguire.demobox.spring.springwebfluxdocumentation.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxdocumentation.repository.Repository

interface RepositoryDependency<SM : StoreableModel> {
    val repository: Repository<SM>
}
