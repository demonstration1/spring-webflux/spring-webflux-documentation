Spring Webflux Documentation
----------------------------
Spring Webflux Documentation is a simple demonstration project to explore automated
documentation generation with Spring REST Docs and AsciiDocs.

### Build & Run
To build the documentation, simply build the application with the following
command:
```
mvn clean package
```

Once built, the documentation can be located at:
http://localhost:8080/docs/documentation.html
